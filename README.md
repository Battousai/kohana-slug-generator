Exemplo:

```

	public function save(Validation $validation = NULL)
    {
        if ($this->changed('titulo')) {
            $this->_setNewSlug();
        }

        parent::save($validation);
    }

    protected function _setNewSlug()
    {
        $this->slug = SlugGenerator::factory($this, $this->titulo, 'slug')
                ->getNewSlug();
    }


```