<?php

class Kohana_SlugGenerator {

    /**
     * @var ORM
     */
    protected $_model;
    /**
     * @var string
     */
    protected $_title;
    /**
     * @var string
     */
    protected $_slugField;
    /**
     * @var string
     */
    protected $_separator;

    public function __construct(ORM $model, $title, $slugField, $separator = '-')
    {
        $this->_model = $model;
        $this->_title = $title;
        $this->_slugField = $slugField;
        $this->_separator = $separator;
    }

    public static function factory(ORM $model, $title, $slugField, $separator = '-')
    {
        return new self($model, $title, $slugField, $separator);
    }

    public function getNewSlug()
    {
        $slug = URL::title($this->_title, $this->_separator, TRUE);
        $appendage = '';
        $count = 0;

        while ($this->_keyExistsButNotSelf($slug.$appendage)) {
            ++$count;
            $appendage = $this->_separator.$count;
        }

        return $slug.$appendage;
    }

    protected function _keyExistsButNotSelf($value)
    {
        return DB::query(
                Database::SELECT,
                'SELECT EXISTS(
                    SELECT NULL
                    FROM '.$this->_model->table_name().'
                    WHERE
                        '.$this->_slugField.' = :value '
                        .$this->_idCheck().'
                ) ex'
            )
            ->param(':value', $value)
            ->as_object(TRUE)
            ->execute()->current()->ex;
    }

    protected function _idCheck()
    {
        return $this->_model->pk() ? ' AND '.$this->_model->primary_key().' != '.$this->_model->pk() : '';
    }

}